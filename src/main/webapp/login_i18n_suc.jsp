<%--
  Created by IntelliJ IDEA.
  User: haohu
  Date: 2019/3/21 0021
  Time: 20:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>login s2 i18n success</title>
</head>
<body leftmargin="30px" topmargin="30px">
<s:debug/>
<br>
wel.msg:<s:text name="wel.msg"/><br>
wel.info:<s:text name="wel.info"><%--s:text开始--%>
    <s:param value="username"/><%--wel.info中的第1个占位符的内容，见src/main/resources/global_zh_CN.properties文件--%>
    <s:param value="pass"/><%--wel.info中的第2个占位符的内容--%>
    <s:param value="age"/><%--wel.info中的第3个占位符的内容--%>
</s:text><%--/s:text结束--%>
</body>
</html>
