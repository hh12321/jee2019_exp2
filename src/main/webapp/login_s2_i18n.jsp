<%--
  Created by IntelliJ IDEA.
  User: haohu
  Date: 2019/3/20
  Time: 13:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>haohu-login</title>
<s:head/>
</head>
<body topmargin="30px" leftmargin="30px">
i18n
<s:form name="loginTag" action="checkLoginByI18N">
    <s:textfield name="username" key="username" requiredLabel="true"/><%--通过加载全局资源文件实现国际化，见src/main/resources/global_zh_CN.properties文件--%>
    <s:password name="pass" key="pass"/>
    <s:textfield name="age" key="age"/>
    <s:radio name="type" list="{'forward','redirect'}" value="forward"/>
    <s:submit key="submit-internation"/>
</s:form>
</body>
</html>
