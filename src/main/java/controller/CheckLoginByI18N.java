package controller;

import com.opensymphony.xwork2.ActionSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckLoginByI18N extends ActionSupport {
    private static final Logger logger= (Logger) LoggerFactory.getLogger(CheckLoginByI18N.class);
    String username,pass,type;
    int age;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void validate(){
        if(username.isEmpty()){
            addFieldError("username",getText("username.empty"));
        }
        if(pass.isEmpty()){
            addFieldError("pass",getText("pass.empty"));
        }
        if(age<0){
            addFieldError("age",getText("age.illegal",new String[]{""+age}));
        }
    }

    public String execute(){
        logger.debug("username is{},pass is{}",username,pass);
        if(username.equalsIgnoreCase("admin")) {
            return type;
        }else{
            return "fail";
        }
    }
}
